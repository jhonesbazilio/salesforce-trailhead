public with sharing class NewCaseListController {
    public NewCaseListController() {

    }

    public List<Case> getNewCases(){
        List<Case> cases = [select ID, CaseNumber from Case where Status='New'];
        return cases;
    }
}
